import 'package:e_library/modules/settings/presentation/controller/settings_controller.dart';
import 'package:get/get.dart';

class SettingsBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(SettingsController());
  }
}