import 'dart:ui';

import 'package:e_library/core/storage/storage_handler.dart';
import 'package:get/get.dart';

class SettingsController extends GetxController {



  void changeLanguage() async{
    if(StorageHandler().lang == 'ar'){
      await StorageHandler().setLocale('en');
    }else{
      await StorageHandler().setLocale('ar');
    }
    Get.updateLocale(StorageHandler().locale);
    update();
  }

  bool get isEn => StorageHandler().lang == 'en';

  String get language => isEn ? 'English' : 'العربية';

}