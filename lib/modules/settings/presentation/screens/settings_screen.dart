import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/settings_binging.dart';
import '../controller/settings_controller.dart';

class SettingsScreen extends GetView<SettingsController> {
  const SettingsScreen({Key? key}) : super(key: key);

  static const name = '/settings';
  static final page = GetPage(
      name: name,
      page: ()=> const SettingsScreen(),
      binding: SettingsBinding()
  );


  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBar: AppBar(
        title: Text('Settings'.tr),
      ),
      body: GetBuilder<SettingsController>(
        builder: (_){
          return Column(
            children: [
              SwitchListTile(
                onChanged: (_){
                  controller.changeLanguage();
                },
                value: controller.isEn,
                title: Text(controller.language),
                subtitle: Text('change language'.tr),
              )
            ],
          );
        },
      ),
    );
  }


}
