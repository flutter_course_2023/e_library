import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/material.dart';
import '../../../../core/consts/app_colors.dart';
import '../../domain/entity/category_entity.dart';

class CategoryView extends StatelessWidget {
  final CategoryEntity model;
  final ValueChanged<CategoryEntity>? onSelect;
  final bool isSelected;
  const CategoryView(this.model,{Key? key,this.onSelect,this.isSelected=false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    return InkWell(
      onTap: (){
        onSelect?.call(model);
      },
      child: Container(
        margin: EdgeInsets.all(0.75.w),
        decoration: BoxDecoration(
          color: AppColors.fontOnLight.withOpacity(0.2),
          borderRadius: BorderRadius.circular(2.5.w),
          border: isSelected ?Border.all(
            color: theme.primaryColor
          ):null
        ),
        alignment: Alignment.center,
        child: Text(
          model.name,
          style: TextStyle(
            color: isSelected ? theme.primaryColor: null
          ),
        ),
      ),
    );
  }
}
