
import 'package:get/get.dart';
import '../../data/data_source/base_create_book_data_source.dart';
import '../../data/data_source/remote_create_book_data_source.dart';
import '../../data/repository/remote_create_book_repositor.dart';
import '../../domain/repository/base_create_book_repository.dart';
import '../../domain/usecase/create_book_usecase.dart';
import '../../domain/usecase/get_categories_usecase.dart';
import 'create_book_controller.dart';

class CreateBookBinding extends Bindings {

  @override
  void dependencies() {
    Get.put<BaseCreateBookDataSource>(RemoteCreateBookDataSource());
    Get.put<BaseCreateBookRepository>(RemoteCreateBookRepository(Get.find()));
    Get.put(CreateBookUseCase(Get.find()));
    Get.put(GetCategoriesUseCase(Get.find()));
    Get.put(CreateBookController(Get.find(),Get.find(),));
  }

}