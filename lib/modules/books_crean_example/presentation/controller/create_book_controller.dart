import 'package:e_library/core/core_components/pop_up.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/data_state/data_state.dart';
import '../../domain/entity/category_entity.dart';
import '../../domain/usecase/create_book_usecase.dart';
import '../../domain/usecase/get_categories_usecase.dart';

class CreateBookController extends GetxController{

  final CreateBookUseCase createUseCase;
  final GetCategoriesUseCase categoriesUseCase;

  CreateBookController(this.createUseCase,this.categoriesUseCase);

  DataState<void> _createDataState = const DataState<void>();

  String get createError => _createDataState.message;
  DataStatus get createStatus => _createDataState.status;

  DataState<List<CategoryEntity>> _categoriesDataState = const DataState<List<CategoryEntity>>(
    status: DataStatus.loading
  );

  String get categoriesError => _categoriesDataState.message;
  DataStatus get categoriesStatus => _categoriesDataState.status;
  List<CategoryEntity> get categories => _categoriesDataState.data!;








  CreateBookParameters get _parameters =>
      CreateBookParameters(
        title: titleController.text,
        categoryId: categoryId!,
        author: authorController.text,
        coverImage: coverImage!,
        description: descriptionController.text,
        pdf: pdf!
      );

  int? categoryId;

  bool isSelected(int i){
    return i == categoryId;
  }

  set selected(int i){
    categoryId = i;
    update();
  }

  String? pdf;
  String? coverImage;

  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final authorController = TextEditingController();
  final GlobalKey<FormState> key = GlobalKey<FormState>();

  bool get _isNotValid =>
      !key.currentState!.validate()
          || categoryId == null
          || pdf == null
          || coverImage == null;

  void create()async{
    if(_isNotValid){
      showSnackBar('invalid input , missing some data');
      return;
    }
    _createDataState = const DataState<void>();
    update();
    _createDataState = await createUseCase(_parameters);
    if(createStatus == DataStatus.error){
      //
    }else {
      //
    }
  }

  void loadCategories()async {
    _categoriesDataState = const DataState<List<CategoryEntity>>(
        status: DataStatus.loading
    );
    update();
    _categoriesDataState = await categoriesUseCase();
    print(_categoriesDataState);
    update();
  }

  @override
  void onReady() {
    super.onReady();
    loadCategories();
  }

  @override
  void dispose() {
    authorController.dispose();
    descriptionController.dispose();
    titleController.dispose();
    super.dispose();
  }
}