import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:e_library/core/core_components/app_submit_button.dart';
import 'package:e_library/core/core_components/app_text_form_feild.dart';
import 'package:e_library/core/core_components/image_selector.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/core_components/pdf_selector.dart';
import '../../../../core/core_components/status_component.dart';
import '../components/category_view.dart';
import '../controller/create_book_binding.dart';
import '../controller/create_book_controller.dart';

class CreateBookScreen extends GetView<CreateBookController> {
  const CreateBookScreen({Key? key}) : super(key: key);

  static const name = '/create_book';
  static final page = GetPage(
      name: name,
      page: ()=> const CreateBookScreen(),
      binding: CreateBookBinding(),
  );


  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBar: AppBar(
        // TODO :: Translate
        title: Text('Create Boot'.tr),
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(delegate: SliverChildListDelegate(
              [
                ImageSelector(),
                PdfSelector(),
              ]
            )
          ),
          GetBuilder<CreateBookController>(
              builder: (_)=>
                  StatusComponent(
                    status: controller.categoriesStatus,
                    onError: (c)=>SliverToBoxAdapter(
                      child: Center(
                        child: Text(controller.categoriesError),
                      ),
                    ),
                    onSuccess: (c) =>SliverGrid(
                        delegate: SliverChildBuilderDelegate(
                                (c,i)=> CategoryView(
                              controller.categories[i],
                              isSelected: controller.isSelected(i),
                              onSelect: (model){
                                controller.selected = i;
                              },
                            ),
                            childCount: controller.categories.length
                        ),
                        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 2.5
                        )
                    ),
                    onInit: (c) =>const SliverToBoxAdapter(
                      child: Center(
                          child: CircularProgressIndicator()
                      ),
                    ),
                    onLoading: (c) =>const SliverToBoxAdapter(
                      child: Center(
                          child: CircularProgressIndicator()
                      ),
                    ),
                  ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.all(2.5.w),
              child: Column(
                children: [
                  AppTextFormField(
                    hint: 'Book Title',
                  ),
                  Padding(
                    padding:EdgeInsets.symmetric(
                      vertical: 2.5.w
                    ),
                    child: AppTextFormField(
                      hint: 'Book Author',
                    ),
                  ),
                  AppTextFormField(
                    hint: 'Description',
                    maxLines: 5,
                  ),
                  SizedBox(
                    height: 2.5.w,
                  ),
                  AppSubmitButton()
                ],
              ),
            ),
          )
        ],
      )
    );
  }
}
