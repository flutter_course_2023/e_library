


import '../../domain/entity/category_entity.dart';

class CategoryModel extends CategoryEntity{

  CategoryModel({
    required super.id,
    required super.name
  });

  factory CategoryModel.fromJson(Map<String,dynamic> json) =>
      CategoryModel(
          id: json['id'],
          name: json['name']
      );
}