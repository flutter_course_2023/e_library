
import '../../domain/usecase/create_book_usecase.dart';
import '../model/category_model.dart';

abstract class BaseCreateBookDataSource {

  Future<void> createBook(CreateBookParameters parameters);

  Future<List<CategoryModel>> getCategories();

}