



import '../../../../core/consts/api_const.dart';
import '../../../../core/network/network_helper.dart';
import '../../domain/usecase/create_book_usecase.dart';
import '../model/category_model.dart';
import 'base_create_book_data_source.dart';

class RemoteCreateBookDataSource extends BaseCreateBookDataSource{
  @override
  Future<void> createBook(CreateBookParameters parameters) async{
    //
  }

  @override
  Future<List<CategoryModel>> getCategories() async {
    var result = await NetworkHelper().get(ApiConst.categories);
    List books = result.data['result'];
    return books.map(
            (e) => CategoryModel.fromJson(e)
    ).toList();
  }

}