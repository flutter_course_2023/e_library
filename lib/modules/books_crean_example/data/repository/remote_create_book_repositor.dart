import 'package:e_library/core/data_state/data_state.dart';
import '../../../../core/handler/handler.dart';
import '../../domain/entity/category_entity.dart';
import '../../domain/repository/base_create_book_repository.dart';
import '../../domain/usecase/create_book_usecase.dart';
import '../data_source/base_create_book_data_source.dart';

class RemoteCreateBookRepository extends BaseCreateBookRepository {

  final BaseCreateBookDataSource dataSource;


  RemoteCreateBookRepository(this.dataSource);

  @override
  Future<DataState<void>> createBook(CreateBookParameters parameters)
    => handle<void>(()=>dataSource.createBook(parameters));

  @override
  Future<DataState<List<CategoryEntity>>> getCategories()
    => handle<List<CategoryEntity>>(dataSource.getCategories);



}