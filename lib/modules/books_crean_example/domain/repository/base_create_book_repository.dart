


import '../../../../core/data_state/data_state.dart';
import '../entity/category_entity.dart';
import '../usecase/create_book_usecase.dart';

abstract class BaseCreateBookRepository {

  Future<DataState<void>> createBook(CreateBookParameters parameters);

  Future<DataState<List<CategoryEntity>>> getCategories();

}