
import '../../../../core/data_state/data_state.dart';
import '../repository/base_create_book_repository.dart';

class CreateBookUseCase {

  final BaseCreateBookRepository repository;
  CreateBookUseCase(this.repository);

  Future<DataState<void>> call(CreateBookParameters parameters) =>
      repository.createBook(parameters);

}

class CreateBookParameters {

  final int categoryId;
  final String pdf;
  final String coverImage;

  final String title;
  final String description;
  final String author;


  CreateBookParameters(
      {
        required this.categoryId,
        required this.pdf,
        required this.coverImage,
        required this.title,
        required this.description,
        required this.author
      });

  Map<String,dynamic> get json =>{
    'category_id':categoryId,
    'title':title,
    'description':description,
    'author':author
  };
  Map<String,String> get files =>{
    'pdf':pdf,
    'cover_image':coverImage
  };
}