
import '../../../../core/data_state/data_state.dart';
import '../entity/category_entity.dart';
import '../repository/base_create_book_repository.dart';

class GetCategoriesUseCase {

  final BaseCreateBookRepository repository;
  GetCategoriesUseCase(this.repository);

  Future<DataState<List<CategoryEntity>>> call() =>
      repository.getCategories();

}