

import 'package:e_library/core/network/network_helper.dart';
import 'package:e_library/core/storage/storage_handler.dart';

import '../../../../core/consts/api_const.dart';

class AuthDataSource {
  static Future<void> register({
    required String name,
    required String email,
    required String password,
    required String? image
})async{
    var response = await NetworkHelper().post(
        ApiConst.register,
        body: {
          'name': name,
          'email': email,
          'password': password
        },
      files: {
          if(image != null)
          'image':image
      }
    );
    String token = response.data['token'];
    await StorageHandler().setToken(token);
  }

  static Future<void> login({
    required String email,
    required String password,
})async{
    var response = await NetworkHelper().post(
        ApiConst.login,
        body: {
          'email': email,
          'password': password,
        }
    );
    String token = response.data['token'];
    await StorageHandler().setToken(token);
  }

}