

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/storage/storage_handler.dart';
import '../../../../books/presentation/screens/books_screen.dart';

class LoginMiddleware extends GetMiddleware{

  @override
  RouteSettings? redirect(String? route) {
    if(StorageHandler().hasToken){
      return const RouteSettings(
        name: BooksScreen.name
      );
    }
    return null;
  }


}