import 'package:e_library/core/data_state/data_state.dart';
import 'package:e_library/modules/auth/data/data_source/auth_data_source.dart';
import 'package:e_library/modules/books/presentation/screens/books_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/core_components/pop_up.dart';
import '../../../../../core/handler/handler.dart';


class LoginController extends GetxController{


  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();


  DataState<void> _dataState = DataState<void>();

  DataStatus get status => _dataState.status;

  void login() async {

    if(!formKey.currentState!.validate())
      return;
    _dataState = const DataState(status: DataStatus.loading);

    showLoader();
    _dataState = await handle<void>(
        () => AuthDataSource.login(
                email: emailController.text,
                password: passController.text
        )
    );
    Get.back();
    if(status == DataStatus.error){
        showSnackBar(_dataState.message);
    }
    else{
      Get.offAllNamed(BooksScreen.name);
    }
  }

  @override
  void dispose() {
    emailController.dispose();
    passController.dispose();
    super.dispose();
  }

}