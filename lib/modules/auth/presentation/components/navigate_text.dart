import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import '../../../../core/consts/app_colors.dart';


class NavigateText extends StatefulWidget {
  NavigateText({Key? key, Function()? onTap, required this.text1, required this.text2}) : super(key: key){
    _gestureRecognizer.onTap = onTap;
  }
  final String text1;
  final String text2;
  final TapGestureRecognizer _gestureRecognizer = TapGestureRecognizer();


  @override
  State<NavigateText> createState() => _NavigateTextState();
}

class _NavigateTextState extends State<NavigateText> {

  @override
  void dispose() {
    widget._gestureRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        style: TextStyle(
            fontSize: 14.sp,
            color: AppColors.fontOnLight
        ),
        children: [
          TextSpan(text:widget.text1),
          TextSpan(
            text:widget.text2,
            style: const TextStyle(
                color: AppColors.darkBlue,
                fontWeight: FontWeight.bold
            ),
            recognizer: widget._gestureRecognizer,
          ),
        ],
      ),
    );
  }
}