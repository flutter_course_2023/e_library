import 'package:e_library/core/consts/app_assets.dart';
import 'package:e_library/core/consts/app_colors.dart';
import 'package:flutter_svg/svg.dart';
import '/core/ui_sizer/app_sizer.dart';
import 'package:flutter/material.dart';

class AuthHeader extends StatelessWidget {
  const AuthHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: _Clipper(),
      child: Container(
        width: double.infinity,
        height: 60.w,
        color: AppColors.blue,
        padding: EdgeInsets.all(10.w).copyWith(
          bottom: 13.5.w,
        ),
        child: SvgPicture.asset(
            AppSVGs.logo,
            color: Colors.white,
        ),
      ),
    );
  }
}

class _Clipper extends CustomClipper<Path>{

  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height*.65);
    path.arcToPoint(
    Offset(size.width, size.height*.65),
    radius: Radius.circular(size.width*.8),
    clockwise: false
    );
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }




}

