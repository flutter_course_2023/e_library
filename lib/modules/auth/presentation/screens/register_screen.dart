import 'package:e_library/core/consts/app_assets.dart';
import 'package:e_library/core/core_components/image_selector_obs.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:e_library/modules/auth/presentation/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../../core/consts/app_colors.dart';
import '../../../../core/core_components/app_scaffold.dart';
import '../../../../core/core_components/app_text_form_feild.dart';
import '../../../../core/core_components/image_selector.dart';
import '../components/auth_header.dart';
import '../../../../core/core_components/app_submit_button.dart';
import '../components/navigate_text.dart';
import '../controller/register/register_binding.dart';
import '../controller/register/register_controller.dart';


class RegisterScreen extends GetView<RegisterController> {
  const RegisterScreen({Key? key}) : super(key: key);

  static const name = '/register';
  static final page = GetPage(
      name: name,
      page: () => const RegisterScreen(),
      binding: RegisterBinding()
  );

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    return AppScaffold(
      body: SingleChildScrollView(
        child: Form(
          key: controller.formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const AuthHeader(),
              Padding(
                padding: EdgeInsets.all(5.w).copyWith(top: 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Register',
                      style: TextStyle(
                          fontSize: 32.sp,
                          color: AppColors.fontOnLight,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                    Center(
                        child:ImageSelector(
                          onSelected: controller.setPath
                        )
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.email_outlined),
                      hint: 'name',
                      controller: controller.nameController,
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'name is required.';
                      },
                    ),
                    SizedBox(
                      height: 3.5.w,
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.email_outlined),
                      hint: 'email',
                      keyboardType: TextInputType.emailAddress,
                      controller: controller.emailController,
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'email is required.';
                        if(!val.isEmail)
                          return 'email is invalid.';
                      },
                    ),
                    SizedBox(
                      height: 3.5.w,
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.lock_outline),
                      hint: 'password',
                      isPass: true,
                      controller: controller.passController,
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'password is required.';
                        if(val.length < 6)
                          return 'password must be at least 6 characters.';
                      },
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.lock_outline),
                      hint: 'confirm password',
                      isPass: true,
                      controller: controller.confirmPassController,
                      validator: (val){
                        if(val == null || val.isEmpty) {
                          return 'password is required.';
                        }
                        if(val.length < 6) {
                          return 'password must be at least 6 characters.';
                        }
                        if(controller.passController.text != controller.confirmPassController.text) {
                          return 'password does not match confirm password';
                        }
                      },
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    AppSubmitButton(
                      onTap: controller.register,
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    NavigateText(
                      text1: 'if you already have an account, you can ',
                      text2: 'Login now.',
                      onTap: ()=>Get.offAllNamed(LoginScreen.name),
                    ),
                    SizedBox(
                      height: mq.viewInsets.bottom,
                    ),

                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}