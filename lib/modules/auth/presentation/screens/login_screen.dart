import 'package:e_library/core/consts/app_assets.dart';
import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import '../../../../core/consts/app_colors.dart';
import '../../../../core/core_components/app_text_form_feild.dart';
import '../components/auth_header.dart';
import '../../../../core/core_components/app_submit_button.dart';
import '../components/navigate_text.dart';
import '../controller/login/login_binding.dart';
import '../controller/login/login_controller.dart';
import '../controller/login/login_middleware.dart';
import 'register_screen.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({Key? key}) : super(key: key);

  static const name = '/';
  static final page = GetPage(
      name: name,
      page: () => const LoginScreen(),
      middlewares: [LoginMiddleware()],
      binding: LoginBinding()
  );

  //LoginController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    return AppScaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const AuthHeader(),
            Padding(
              padding: EdgeInsets.all(5.w).copyWith(top: 0),
              child: Form(
                key: controller.formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Login',
                      style: TextStyle(
                          fontSize: 32.sp,
                          color: AppColors.fontOnLight,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(
                      height: 7.5.w,
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.email_outlined),
                      hint: 'email',
                      keyboardType: TextInputType.emailAddress,
                      controller: controller.emailController,
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'required';
                        if(!val.isEmail)
                          return 'invalid email form';
                      },
                    ),
                    SizedBox(
                      height: 3.5.w,
                    ),
                    AppTextFormField(
                      icon: Icon(Icons.lock_outline),
                      hint: 'password',
                      isPass: true,
                      controller: controller.passController,
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'required';
                        if(val.length < 6)
                          return 'password must be at least 6 characters';
                      },
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    AppSubmitButton(
                        onTap: controller.login,
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    NavigateText(
                      text1: 'if you don\'t have an account, you can ',
                      text2: 'Register now.',
                      onTap: ()=>Get.offAllNamed(RegisterScreen.name),
                    ),
                    SizedBox(
                      height: mq.viewInsets.bottom,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
