import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:e_library/core/core_components/status_component.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:e_library/modules/books/presentation/components/category_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controller/search/search_filter_controller.dart';
import '../../../../core/consts/app_colors.dart';
import '../controller/search/search_filter_binding.dart';

class SearchFilterScreen extends GetView<SearchFilterController> {
  const SearchFilterScreen({Key? key}) : super(key: key);

  static const name = '/search_filter';
  static final page = GetPage(
    name: name,
    page: ()=> const SearchFilterScreen(),
    binding: SearchFilterBinding()
  );

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBar: AppBar(
        title: Text('Search'.tr),
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(delegate: SliverChildListDelegate([
            Padding(
              padding: EdgeInsets.all(2.5.w),
              child: TextField(
                decoration: InputDecoration(
                    fillColor: AppColors.fontOnLight.withOpacity(0.2),
                    filled: true,
                    border: OutlineInputBorder(
                      borderSide: BorderSide.none,
                      borderRadius: BorderRadius.circular(2.5.w),
                    ),
                    hintText: 'Type book title to search'.tr,
                    prefixIcon: const Icon(CupertinoIcons.search)
                ),
                controller: controller.searchBoxController,
                textInputAction: TextInputAction.search,
                onEditingComplete: controller.showSearchResult
              ),
            ),
            // TODO : translate
            Padding(
              padding: EdgeInsets.all(2.5.w),
              child: Text('Select category : '),
            ),
          ])),
          GetBuilder<SearchFilterController>(
            builder: (_)=>StatusComponent(
                status: controller.status,
                onError: (c)=>SliverToBoxAdapter(
                  child: Center(
                    child: Text(controller.error),
                  ),
                ),
                onSuccess: (c) =>SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                            (c,i)=> CategoryView(
                                controller.data[i],
                                isSelected: controller.isSelected(i),
                                onSelect: (model){
                                  controller.selected = i;
                                },
                            ),
                            childCount: controller.data.length
                    ),
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        childAspectRatio: 2.5
                    )
                ),
                onInit: (c) =>const SliverToBoxAdapter(
                  child: Center(
                    child: CircularProgressIndicator()
                  ),
                ),
                onLoading: (c) =>const SliverToBoxAdapter(
                  child: Center(
                      child: CircularProgressIndicator()
                  ),
                ),
            ),
          )
        ],
      ),
    );
  }
}
