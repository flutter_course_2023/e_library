import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:e_library/core/core_components/status_component.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:e_library/modules/books/presentation/screens/read_book_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/consts/app_colors.dart';
import '../../../../core/core_components/app_network_image.dart';
import '../components/comment_view.dart';
import '../components/sliver_header.dart';
import '../controller/book_info/book_info_binding.dart';
import '../controller/book_info/book_info_controller.dart';

class BookInfoScreen extends GetView<BookInfoController> {
  const BookInfoScreen({Key? key}) : super(key: key);

  static const _name = '/book_info';
  static final page = GetPage(
      name: _name,
      page: ()=> const BookInfoScreen(),
      binding: BookInfoBinding()
  );

  static void navigate(int id){
    Get.toNamed(_name,arguments: id);
  }

  @override
  Widget build(BuildContext context) {
    var mq = MediaQuery.of(context);
    return AppScaffold(
      body: GetBuilder<BookInfoController>(
        builder: (_) {
          return StatusComponent(
            status: controller.status,
            onError: (_) => Center(
              child: Text(controller.error),
            ),
            onSuccess: (_) => CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: [
                SliverAppBar(
                  pinned: true,
                  title: Text(controller.data.title),
                ),
                SliverToBoxAdapter(
                  child: SizedBox(
                    height: 5.w,
                  ),
                ),
                AppSliverHeader(
                  height: 70.w,
                  child: AppNetworkImage(
                    controller.data.coverImage,
                  ),
                ),
                SliverList(
                    delegate: SliverChildListDelegate(
                      [
                        SizedBox(
                          height: 5.w,
                        ),
                        ListTile(
                          title: Text('Author'.tr),
                          subtitle: Text(controller.data.author),
                        ),
                        ListTile(
                          title: Text('Category'.tr),
                          subtitle: Text(controller.data.category!.name),
                        ),
                        ListTile(
                          leading: const Icon(Icons.book),
                          //Todo : Translate
                          title: Text('Read Now'.tr),
                          onTap: () => ReadBookScreen.navigate(controller.data),
                        ),
                        ListTile(
                          title: Text('${controller.data.likes} ${'Likes'.tr}'),
                          trailing: Visibility(
                            visible: controller.data.isLike,
                            replacement: IconButton(
                              icon:Icon(Icons.thumb_up_alt_outlined),
                              onPressed: controller.like,
                            ),
                            child: IconButton(
                              icon:Icon(Icons.thumb_down_alt_outlined),
                              onPressed: controller.unlike,
                            ),
                          )
                        ),
                        ListTile(
                          title: Text('Description'.tr),
                          subtitle: Text(controller.data.description),
                        ),
                        Divider(
                          color: AppColors.fontOnLight,
                        ),
                        ListTile(
                          title: Text('${'Comments'.tr} :'),
                          leading: Icon(Icons.comment_outlined),
                        ),
                      ]
                    )
                ),
                SliverList(
                    delegate: SliverChildBuilderDelegate(
                            (context, i) => CommentView(controller.data.comments[i]),
                        childCount: controller.data.comments.length
                    )
                ),
                SliverList(
                    delegate: SliverChildListDelegate(
                        [
                          Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 5.w,
                              horizontal: 2.5.w
                            ),
                            child: TextField(
                              controller: controller.commentController,
                              decoration: InputDecoration(
                                hintText: 'Write your comment'.tr,
                                suffixIcon: IconButton(
                                  icon: const Icon(Icons.send),
                                  onPressed: controller.comment,
                                )
                              ),
                            ),
                          ),
                          SizedBox(
                            height: mq.viewInsets.bottom,
                          )
                        ]
                    )
                ),
              ],
            ),
          );
        }
      ),
    );
  }
}
