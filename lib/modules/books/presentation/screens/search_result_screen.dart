import 'package:e_library/core/core_components/app_scaffold.dart';
import 'package:e_library/core/core_components/status_component.dart';
import 'package:e_library/modules/books/presentation/components/book_view.dart';
import 'package:e_library/modules/books/presentation/controller/search_result/search_result_binding.dart';
import 'package:e_library/modules/books/presentation/controller/search_result/search_result_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchResultScreen extends GetView<SearchResultController> {
  const SearchResultScreen({Key? key}) : super(key: key);

  static const _name = '/search_result';
  static final page = GetPage(
      name: _name,
      page: ()=>const SearchResultScreen(),
      binding: SearchResultBinding(),
  );

  static void navigate(int? categoryId,String title){
    Get.toNamed(_name,arguments: {
      'category_id' : categoryId,
      'title':title
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      appBar: AppBar(
        title: Text(controller.title),
      ),
      body: GetBuilder<SearchResultController>(
        builder: (_)=>StatusComponent(
            status: controller.status,
            onError: (c) => Center(
              child: Text(controller.error),
            ),
            onSuccess: (c) => ListView.builder(
              itemCount: controller.data.length,
              itemBuilder: (c,i)=> BookView(controller.data[i]),
            )
        ),
      ),
    );
  }
}
