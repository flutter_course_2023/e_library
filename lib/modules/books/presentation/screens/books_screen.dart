import 'package:e_library/core/consts/app_assets.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../core/consts/app_colors.dart';
import '../../../../core/core_components/app_drawer.dart';
import '../../../../core/core_components/app_scaffold.dart';
import '../../../../core/core_components/status_component.dart';
import '../../../../core/data_state/data_state.dart';
import '../../../settings/presentation/screens/settings_screen.dart';
import '../components/book_view.dart';
import '../components/search_box.dart';
import '../controller/books/books_binding.dart';
import '../controller/books/books_controller.dart';

class BooksScreen extends GetView<BooksController> {
  const BooksScreen({Key? key}) : super(key: key);

  static const name = '/books';
  static final page = GetPage(
      name: name, page: () => const BooksScreen(), binding: BooksBinding());

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
        drawer: const AppDrawer(),
        body: GetBuilder<BooksController>(builder: (_) {
          return StatusComponent(
        status: controller.status,
        onSuccess: (context) => CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverAppBar(
              title: Text('Home'.tr),
              pinned: true,
            ),
            SliverPersistentHeader(
              delegate: SearchBoxDelegate(),
              pinned: true,
            ),
            SliverList(
                delegate: SliverChildBuilderDelegate(
                    (c, i) => BookView(controller.data[i]),
                    childCount: controller.data.length)),
          ],
        ),
        onError: (context) => SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(controller.error),
              ElevatedButton(
                  onPressed: controller.loadData, child: Text("TRY AGAIN".tr))
            ],
          ),
        ),
      );
        })
    );
  }
}
