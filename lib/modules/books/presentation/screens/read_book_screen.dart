import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

import '../../data/models/book_model.dart';

class ReadBookScreen extends StatelessWidget {

  final BookModel model;
  const ReadBookScreen(this.model,{Key? key}) : super(key: key);

  static void navigate(BookModel model){
    Get.to(() => ReadBookScreen(model));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(model.title),
      ),
      body: SfPdfViewer.network(
          model.pdf
      ),
    );
  }
}
