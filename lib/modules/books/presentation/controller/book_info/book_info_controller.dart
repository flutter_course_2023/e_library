import 'package:e_library/core/data_state/data_state.dart';
import 'package:e_library/core/handler/handler.dart';
import 'package:e_library/modules/books/data/data_source/books_data_source.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../core/core_components/pop_up.dart';
import '../../../data/models/book_model.dart';

class BookInfoController extends GetxController{
  final int bookId;
  final TextEditingController commentController = TextEditingController();

  BookInfoController(this.bookId);

  DataState<BookModel> _dataState =const DataState<BookModel>(
    status: DataStatus.loading
  );

  DataStatus get status => _dataState.status;
  BookModel get data => _dataState.data!;
  String get error => _dataState.message;

  void loadData() async {
    _dataState = const DataState<BookModel>(
        status: DataStatus.loading
    );
    update();
    _dataState = await handle<BookModel>(() => BooksDataSource.getBookById(bookId));
    update();
    print(_dataState.data?.isLike);
  }

  @override
  void onReady() {
    super.onReady();
    loadData();
  }

  void like()async{
    showLoader();
    DataState<void> likeState =
        await handle(() => BooksDataSource.likeBook(data.id));
    Get.back();
    if(likeState.status == DataStatus.error){
      showSnackBar(likeState.message);
    }else{
      showSnackBar('book liked !!');
      loadData();
    }
  }

  void unlike()async{
    showLoader();
    DataState<void> unlikeState =
    await handle(() => BooksDataSource.unlikeBook(data.id));
    Get.back();
    if(unlikeState.status == DataStatus.error){
      showSnackBar(unlikeState.message);
    }else{
      showSnackBar('book unliked !!');
      loadData();
    }
  }

  void comment()async{
    if(commentController.text.isEmpty){
      showSnackBar('comment cannot be empty!');
      return;
    }
    showLoader();
    DataState<void> likeState =
        await handle(() => BooksDataSource.comment(data.id,commentController.text)
        );
    Get.back();
    if(likeState.status == DataStatus.error){
      showSnackBar(likeState.message);
    }else{
      showSnackBar('comment written successfully!!');
      loadData();
      commentController.text = '';
    }
  }

  @override
  void dispose() {
    commentController.dispose();
    super.dispose();
  }

}