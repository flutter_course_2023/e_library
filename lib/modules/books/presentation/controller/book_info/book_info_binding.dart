import 'package:e_library/modules/books/presentation/controller/book_info/book_info_controller.dart';
import 'package:get/get.dart';

class BookInfoBinding extends Bindings {

  @override
  void dependencies() {
    Get.put(BookInfoController(Get.arguments));
  }

}