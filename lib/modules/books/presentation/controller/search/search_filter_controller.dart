import 'package:e_library/core/data_state/data_state.dart';
import 'package:e_library/modules/books/data/data_source/books_data_source.dart';
import 'package:e_library/modules/books/data/models/book_model.dart';
import 'package:e_library/modules/books/data/models/category_model.dart';
import 'package:e_library/modules/books/presentation/screens/search_result_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../core/handler/handler.dart';

class SearchFilterController extends GetxController{


  DataState<List<CategoryModel>> _categoriesState = DataState<List<CategoryModel>>();
  final searchBoxController = TextEditingController();


  List<CategoryModel> get data => _categoriesState.data!;
  String get error => _categoriesState.message;
  DataStatus get status => _categoriesState.status;

  int? _selected;

  bool isSelected(int i){
    return i == _selected;
  }

  set selected(int i){
    _selected = i;
    update();
  }



  void loadData()async{
    _categoriesState = const DataState<List<CategoryModel>>(
      status: DataStatus.loading
    );
    update();
    _categoriesState = await handle<List<CategoryModel>>(BooksDataSource.getCategories);
    update();
  }

  @override
  void onReady() {
    super.onReady();
    loadData();
  }

  void showSearchResult(){
    if(_selected != null) {
      SearchResultScreen.navigate(
          data[_selected!].id, searchBoxController.text);
    }else{
      SearchResultScreen.navigate(null, searchBoxController.text);
    }
  }


  @override
  void dispose() {
    searchBoxController.dispose();
    super.dispose();
  }
}