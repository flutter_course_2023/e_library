import 'package:e_library/modules/books/presentation/controller/search/search_filter_controller.dart';
import 'package:get/get.dart';

class SearchFilterBinding extends Bindings {


  @override
  void dependencies() {
    //Get.lazyPut(() => SearchController());
    Get.put(SearchFilterController());
  }
}