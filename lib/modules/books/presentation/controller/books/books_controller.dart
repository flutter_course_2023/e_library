import 'package:e_library/core/handler/handler.dart';
import 'package:e_library/modules/books/data/data_source/books_data_source.dart';
import 'package:e_library/modules/books/data/models/book_model.dart';
import 'package:get/get.dart';

import '../../../../../core/data_state/data_state.dart';



class BooksController extends GetxController{

  DataState<List<BookModel>> _dataState = const DataState(
    status: DataStatus.loading
  );

  List<BookModel> get data => _dataState.data!;

  String get error => _dataState.message;

  DataStatus get status => _dataState.status;

  void loadData() async{
    _dataState = DataState<List<BookModel>>(status: DataStatus.loading);
    update();
    _dataState = await handle<List<BookModel>>(BooksDataSource.getBooks);
    update();
  }

  @override
  void onReady() {
    super.onReady();
    loadData();
  }

}