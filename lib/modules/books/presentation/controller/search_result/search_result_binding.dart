import 'package:e_library/modules/books/presentation/controller/search_result/search_result_controller.dart';
import 'package:get/get.dart';

class SearchResultBinding extends Bindings{


  @override
  void dependencies() {
    Get.put(SearchResultController(Get.arguments['category_id'],Get.arguments['title']));
  }
}