import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import '../../../../../core/data_state/data_state.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/data_source/books_data_source.dart';
import '../../../data/models/book_model.dart';

class SearchResultController extends GetxController{

  final int? categoryId;
  final String title;


  SearchResultController(this.categoryId, this.title);

  DataState<List<BookModel>> _dataState = const DataState(
      status: DataStatus.loading
  );

  List<BookModel> get data => _dataState.data!;

  String get error => _dataState.message;

  DataStatus get status => _dataState.status;

  void loadData() async{
    _dataState = const DataState<List<BookModel>>(status: DataStatus.loading);
    update();
    _dataState = await handle<List<BookModel>>(()=>BooksDataSource.getBooks(
      categoryId: categoryId,
      title: title.isEmpty ? null : title
    ));
    update();
  }

  @override
  void onReady() {
    super.onReady();
    loadData();
  }

}