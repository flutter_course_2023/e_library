import 'package:e_library/core/consts/app_colors.dart';
import 'package:e_library/core/core_components/app_network_image.dart';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:e_library/modules/books/data/models/book_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../screens/book_info_screen.dart';

class BookView extends StatelessWidget {
  final BookModel model;
  const BookView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        height: 35.w,
        width: double.infinity,
        child: Row(
          children: [
            Expanded(
                flex: 2,
                child: AppNetworkImage(
                  model.coverImage
                )
            ),
            Expanded(
                flex: 5,
                child: Column(
                  children: [
                    ListTile(
                      title: Text(model.title),
                      subtitle: Text(model.author),
                    ),
                    ListTile(
                      leading: const Icon(Icons.thumb_up),
                      title: Text(model.likes.toString()),
                    ),
                  ],
                )
            ),
            Expanded(
                child: IconButton(
                  icon: Icon(Icons.arrow_forward_ios,color: AppColors.fontOnLight.withOpacity(0.7),),
                  onPressed: ()=> BookInfoScreen.navigate(model.id),
                )
            ),
          ],
        ),
      ),
    );
  }
}


