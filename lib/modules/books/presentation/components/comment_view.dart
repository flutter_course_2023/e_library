import 'package:e_library/modules/books/data/models/comment_model.dart';
import 'package:flutter/material.dart';

import '../../../../core/consts/app_colors.dart';
import '../../../../core/core_components/app_network_image.dart';

class CommentView extends StatelessWidget {
  final CommentModel model;
  const CommentView(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      textColor: AppColors.fontOnLight,
      title: Text(model.user.name),
      subtitle: Text(model.comment),
      leading: CircleAvatar(
        backgroundColor: AppColors.fontOnLight.withOpacity(0.25),
        foregroundColor: AppColors.fontOnLight,
        child: model.user.image != null ?
            AppNetworkImage(
                model.user.image!,
                shape: BoxShape.circle,
            )
            : Icon(Icons.person_outline),
      ),
    );
  }
}
