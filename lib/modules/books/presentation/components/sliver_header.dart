import 'package:flutter/material.dart';

class AppSliverHeader extends StatelessWidget {
  final double height;
  final Widget child;
  const AppSliverHeader({Key? key,required this.height, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      delegate: _AppHeaderDelegate(child,height),
      pinned: true,
    );
  }
}

class _AppHeaderDelegate extends SliverPersistentHeaderDelegate{

  final Widget child;
  final double height;

  _AppHeaderDelegate(this.child,this.height);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    var o = 1 - shrinkOffset/maxExtent;
    if(maxExtent - shrinkOffset < maxExtent*0.75) o = 0;
    return Opacity(
      opacity: o,
      child: child
    );
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => height;

  @override
  // TODO: implement minExtent
  double get minExtent => height*0.2;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }



}
