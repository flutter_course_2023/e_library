
import 'package:e_library/core/consts/api_const.dart';
import 'package:e_library/core/network/network_helper.dart';

import '../models/book_model.dart';
import '../models/category_model.dart';

class BooksDataSource {

  static Future<List<BookModel>> getBooks({String? title,int? categoryId}) async {
    var result = await NetworkHelper().get(ApiConst.getAllBooks(
        title:title,
        categoryId: categoryId
    ));
    List books = result.data['result'];
    return books.map(
            (e) => BookModel.fromJson(e)
    ).toList();
  }

  static Future<List<CategoryModel>> getCategories() async {
    var result = await NetworkHelper().get(ApiConst.categories);
    List books = result.data['result'];
    return books.map(
            (e) => CategoryModel.fromJson(e)
    ).toList();
  }

  static Future<BookModel> getBookById(int id) async {
    var result = await NetworkHelper().get(ApiConst.getBookById(id));
    return BookModel.fromJson(result.data['result']);
  }

  static Future<void> likeBook(int id)async{
    await NetworkHelper().post(
        ApiConst.like,
        body: {
          "book_id":id
        }
    );
  }

  static Future<void> unlikeBook(int id)async{
    await NetworkHelper().delete(ApiConst.unlike(id),);
  }

  static Future<void> comment(int id,String comment)async{
    await NetworkHelper().post(
        ApiConst.comment,
        body: {
          "book_id":id,
          "comment":comment
        }
    );
  }

  static Future<void> createBook({
    required String pdf,
    required int categoryId,
    required String title,
    required String author,
    required String description,
    required String coverImage,
})async{
    await NetworkHelper().post(
      ApiConst.createBook,
      body: {
        'category_id':categoryId,
        'title':title,
        'author':author,
        'description':description,
      },
      files: {
        'pdf':pdf,
        'cover_image':coverImage
      }
    );
  }


}