
class UserModel {

  final int id;
  final String name;
  final String? image;

  UserModel({
    required this.id,
    required this.name,
    required this.image
  });

  factory UserModel.fromJson(Map<String,dynamic> json)
    => UserModel(
        id: json['id'],
        name: json['name'],
        image: json['image']
    );
}