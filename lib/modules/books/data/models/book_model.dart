

import 'category_model.dart';
import 'comment_model.dart';

class BookModel {

  final int id;
  final String title;
  final String author;
  final String description;
  final String coverImage;
  final String pdf;
  final int likes;
  final DateTime createdAt;
  final CategoryModel? category;
  final List<CommentModel> comments;
  final bool isLike;

  BookModel({
      required this.id,
      required this.title,
      required this.author,
      required this.description,
      required this.coverImage,
      required this.pdf,
      required this.likes,
      required this.createdAt,
      required this.category,
      required this.comments,
      required this.isLike
  });

  factory BookModel.fromJson(Map<String,dynamic> json)=>
      BookModel(
          id: json['id'],
          title: json['title'],
          author: json['author'],
          description: json['description'],
          coverImage: json['cover_image'],
          pdf: json['pdf'],
          likes: json['likes'],
          createdAt: DateTime.parse(json['created_at']),
          category: json['category'] != null ?
                    CategoryModel.fromJson(json['category']):null,
          comments: ((json['comments'] as List?) ?? []).map((e) => CommentModel.fromJson(e)).toList(),
          isLike: (json['is_liked'] as List?)?.isNotEmpty ?? false
      );

}