
import 'user_model.dart';

class CommentModel {

  final int id;
  final String comment;
  final UserModel user;

  CommentModel({required this.id,required this.comment,required this.user});

  factory CommentModel.fromJson(Map<String,dynamic> json) =>
      CommentModel(
          id: json['id'],
          comment: json['comment'],
          user: UserModel.fromJson(json['user'])
      );
}