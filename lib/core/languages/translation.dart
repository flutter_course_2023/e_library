import 'package:get/get.dart';

class AppTranslation extends Translations {


  @override
  Map<String, Map<String, String>> get keys => {
    'en_us':{
      'Home':'Home',
      "TRY AGAIN":"TRY AGAIN",
      'Author':'Author',
      'Category':'Category',
      'Description':'Description',
      'Likes':'Likes',
      'Comments':'Comments',
      'Write your comment':'Write your comment',
      'Search':'Search',
      'change language':'change language'
    },
    'ar_sy':{
      'Home':'الرئيسية',
      "TRY AGAIN":'حاول مجدداً',
      'Author':'الكاتب',
      'Category':'التصنيف',
      'Description':'الوصف',
      'Likes':'الاعجابات',
      'Comments':'التعليقات',
      'Write your comment':'اكتب تعليق',
      'Search':'البحث',
      'Profile':'الملف الشخصي',
      'Logout':'تسجيل الخروج',
      'Settings':'الاعدادات',
      'change language':'تغيير اللغة',
      'Type book title to search':'للبحث اكتب اسم الكتاب'
    },
  };
}