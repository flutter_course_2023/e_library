import 'package:flutter/material.dart';

import '../data_state/data_state.dart';

class StatusComponent extends StatelessWidget {

  final DataStatus status;
  final WidgetBuilder onError;
  final WidgetBuilder onSuccess;
  final WidgetBuilder? onLoading;
  final WidgetBuilder? onInit;


  const StatusComponent({
    Key? key,
    required this.status,
    required this.onError,
    required this.onSuccess,
    this.onLoading,
    this.onInit
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch(status){
      case DataStatus.error:
        return onError(context);
      case DataStatus.success:
        return onSuccess(context);
      case DataStatus.init:
        return onInit?.call(context) ?? const Center(
          child: CircularProgressIndicator(),
        );
      case DataStatus.loading:
        return onLoading?.call(context) ?? const Center(
          child: CircularProgressIndicator(),
        );
    }
  }
}