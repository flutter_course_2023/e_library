import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/material.dart';

import '../consts/app_colors.dart';

class AppSubmitButton extends StatelessWidget {
  const AppSubmitButton({Key? key,this.onTap,this.label = 'Submit'}) : super(key: key);

  final Function()? onTap;
  final String label;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 12.5.w,
        decoration: BoxDecoration(
          color: AppColors.blue,
          borderRadius: BorderRadius.circular(7.5.w),
        ),
        alignment: Alignment.center,
        child: Text(
          label,
          style: TextStyle(
            fontSize: 18.sp,
            color: Colors.white,
            fontWeight: FontWeight.w500
          ),
        ),
      ),
    );
  }
}
