import 'dart:io';
import 'package:e_library/core/ui_sizer/app_sizer.dart';
import 'package:flutter/material.dart';
import '../consts/app_colors.dart';
import '../file_manager/file_manager.dart';

class PdfSelector extends StatefulWidget {
  const PdfSelector({Key? key, this.onSelected}) : super(key: key);
  final ValueChanged<String?>? onSelected;
  @override
  State<PdfSelector> createState() => _PdfSelectorState();
}

class _PdfSelectorState extends State<PdfSelector> {

  String? path;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      // TODO :: Translate
      title: Text(path?.split("/").lastOrNull ?? 'Select PDF File'),
      onTap: _tap,
      trailing: const Icon(Icons.file_open_outlined),
    );
  }

  void _tap()async{
    path = await FileManager().getFilePdfPath();
    setState(() {
      widget.onSelected?.call(path);
    });
  }

}
