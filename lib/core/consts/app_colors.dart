
import 'package:flutter/material.dart';

class AppColors {

  static const blue = Color(0xFF387ADB);
  static const lightBlue = Color(0xFF75a2e6);
  static const darkBlue = Color(0xFF1e54a4);

  static final fontOnLight = Colors.grey[800]!;
  static const red = Colors.red;



}