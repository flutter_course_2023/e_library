
class AppSVGs{
  static const _base = 'assets/svg';

  static const gridBackground = '$_base/grid_background.svg';
  static const logo = '$_base/logo.svg';

}