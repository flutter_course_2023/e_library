
class ApiConst {

  static const _baseUrl = 'http://10.0.2.2:8000/api';

  static const login = '$_baseUrl/auth/login';

  static const register = '$_baseUrl/auth/register';

  static const _allBooks = '$_baseUrl/books';

  static const createBook = _allBooks;

  static const like = '$_baseUrl/likes';

  static String unlike(id) => '$_baseUrl/likes/$id';

  static const comment = '$_baseUrl/comments';

  static const categories = '$_baseUrl/categories';

  static String getAllBooks({String? title,int? categoryId}) =>
      '$_allBooks${_prams({
        'title':title,
        'category_id':categoryId
      })}';

  static String _pram(String key,value){
    if(value == null)
      return '';
    return '$key=$value';
  }

  static String _prams(Map<String,dynamic> prams){
    var filteredPrams = prams.map((key, value) => MapEntry(key, value));
    filteredPrams.removeWhere((key, value) => value == null);
    if(filteredPrams.isEmpty)
      return '';

    List<String> keys = filteredPrams.keys.toList();
    String result = '?${keys.first}=${filteredPrams[keys.first]}';
    for(int i = 1;i < keys.length ; i++){
      result = '$result&${keys[i]}=${filteredPrams[keys[i]]}' ;
    }
    return result;
  }

  static String getBookById(int id) => '$_allBooks/$id';


}