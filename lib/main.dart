import 'package:e_library/core/storage/storage_handler.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'core/consts/app_colors.dart';
import 'core/languages/translation.dart';
import 'modules/auth/presentation/screens/login_screen.dart';
import 'modules/auth/presentation/screens/register_screen.dart';
import 'modules/books/presentation/screens/book_info_screen.dart';
import 'modules/books/presentation/screens/books_screen.dart';
import 'modules/books/presentation/screens/search_filter_screen.dart';
import 'modules/books/presentation/screens/search_result_screen.dart';
import 'modules/books_crean_example/presentation/screens/create_book_screen.dart';
import 'modules/settings/presentation/screens/settings_screen.dart';

void main() async{
  await StorageHandler.init();
  //await StorageHandler().removeToken();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          elevation: 0,
          color: AppColors.fontOnLight.withOpacity(0.2),
          centerTitle: true,
          foregroundColor: AppColors.fontOnLight,
        ),
        //brightness: Brightness.dark
      ),
      translations: AppTranslation(),
      locale: StorageHandler().locale,
      getPages: [
        BooksScreen.page,
        LoginScreen.page,
        RegisterScreen.page,
        BookInfoScreen.page,
        SettingsScreen.page,
        SearchFilterScreen.page,
        SearchResultScreen.page,
        CreateBookScreen.page
      ],

    );
  }
}
